// Defines the table design of table `Microphones` in database.sqlite
module.exports = (sequelize, DataTypes) => {
  // Initial version
  const microphones = sequelize.define('microphones',
    // Initial version
    /*{
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      }
    },*/
    // Read all data fields in database
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      manufacturer: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      model: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      serial_no: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      sensitivity: {
        type: DataTypes.FLOAT,
        allowNull: false
      },
    },
    {
      tableName: 'microphones',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });

  return microphones;
};
