// Defines the table design of table `loudspeakers` in database.sqlite
module.exports = (sequelize, DataTypes) => {
  // Initial version
  const loudspeakers = sequelize.define('loudspeakers',
    // Read all data fields in database
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      manufacturer: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      model: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      serial_no: {
        type: DataTypes.TEXT(255),
        allowNull: false
      },
      boxtype: {
        type: DataTypes.TEXT(20),
        allowNull: false
      },
    },
    {
      tableName: 'loudspeakers',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });

  return loudspeakers;
};
