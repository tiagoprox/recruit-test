const express = require('express');
const connection = require('../setup/database');

const LoudspeakerModel = connection.import('../models/loudspeaker');
const router = express.Router();

// GET: Finds an entry in the loudspeakers table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return LoudspeakerModel.findOne({ where: { id } })
    .then((loudspeaker) => res.status(200).send(loudspeaker))
    .catch((error) => res.status(500).send({ error }));
});

// POST: Creates a new entry in loudspeakers
router.post('/', (req, res) => {
  // Get body of post request and store necessary values
  const obj = {
    manufacturer: req.body.manufacturer,
    model: req.body.model,
    serial_no: req.body.serial_no,
    boxtype: checkBoxtype(req.body.boxtype)
  };

  if (obj.boxtype == null)
    return res.status(400).send( { code: 0, message: "boxtype invalid. Insert only '1-way', '2-way' or '3-way'" });

  // Add body data to DB
  return LoudspeakerModel.create(obj)
    .then((loudspeaker) => res.status(200).send(loudspeaker))
    .catch((error) => res.status(500).send({ error }));
});

// POST FOR UPDATE: Updated values for a certain ID in loudspeakers
router.post('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  // Get body of post request and store necessary values
  const obj = {
    manufacturer: req.body.manufacturer,
    model: req.body.model,
    serial_no: req.body.serial_no,
    boxtype: checkBoxtype(req.body.boxtype)
  };

  if (obj.boxtype == null)
    return res.status(400).send( { code: 0, message: "boxtype invalid. Insert only '1-way', '2-way' or '3-way'" });

  // Add body data to DB
  return LoudspeakerModel.update(obj, { where: { id } })
    .then((loudspeaker) => {
      if (loudspeaker[0] > 0)
      {
        res.status(200).send({rows: loudspeaker[0], status:"Updated successfully"})
      }
      else {
        res.status(200).send({rows: loudspeaker[0], status:"No rows updated"})
      }
    })
    .catch((error) => res.status(500).send({ error }));
});


// DELETE: Delete a entry in loudspeakers
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return LoudspeakerModel.destroy({ where: { id } })
    .then((loudspeaker) => {
      if (loudspeaker === 1)
      {
        res.status(200).send({code: loudspeaker, status:"Deleted successfully"})
      }
      else
      {
        res.status(200).send({code: 0, status:"No rows deleted"})
      }
    })
    .catch((error) => res.status(500).send({ error }));
});


// Valid values for boxtype
const validBoxtypes = ["1-way","2-way","3-way"];
// Check if boxtype value is valid
function checkBoxtype(boxtype)
{
    // Using for loop
    /*var isValid = false;

    for(boxtypeValue of validBoxtypes)
    {
      if (String(boxtype).toLowerCase() == boxtypeValue)
        isValid = true;
    }*/
    // Using includes()
    var isValid = validBoxtypes.includes(String(boxtype).toLowerCase());

    // If the string is valid return the value as it is
    // If the string is not valid set it to null to force an error
    return isValid ? boxtype : null;
}

module.exports = router;
