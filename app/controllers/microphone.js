const express = require('express');
const connection = require('../setup/database');

const MicrophoneModel = connection.import('../models/microphone');
const router = express.Router();

// GET: Finds an entry in the Microphones table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return MicrophoneModel.findOne({ where: { id } })
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});

// POST: Creates a new entry in Microphones
router.post('/', (req, res) => {
  // const { body } = req;
  //
  // //return res.status(200).send(body);

  // Get body of post request and store necessary values
  const obj = {
    manufacturer: req.body.manufacturer,
    model: req.body.model,
    serial_no: req.body.serial_no,
    sensitivity: (!isNaN(req.body.sensitivity) && (typeof req.body.sensitivity !== "boolean")) ? req.body.sensitivity : null // if sensitivity is not a number : defines the value to null to report an error
  };

  //console.dir(obj);

  if (obj.sensitivity == null)
    return res.status(400).send( { code: 0, message: "sensitivity invalid. Insert only numbers" });

  // Add body data to DB
  return MicrophoneModel.create(obj)
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});

// POST FOR UPDATE: Updated values for a certain ID in Microphones
router.post('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  // Get body of post request and store necessary values
  const obj = {
    manufacturer: req.body.manufacturer,
    model: req.body.model,
    serial_no: req.body.serial_no,
    sensitivity: (!isNaN(req.body.sensitivity) && (typeof req.body.sensitivity !== "boolean")) ? req.body.sensitivity : null // if sensitivity is not a number : defines the value to null to report an error
  };

  if (obj.sensitivity == null)
    return res.status(400).send( { code: 0, message: "sensitivity invalid. Insert only numbers" });

  // Add body data to DB
  return MicrophoneModel.update(obj, { where: { id } })
    .then((microphone) => {
      if (microphone[0] > 0)
      {
        res.status(200).send({rows: microphone[0], status:"Updated successfully"})
      }
      else {
        res.status(200).send({rows: microphone[0], status:"No rows updated"})
      }
    })
    .catch((error) => res.status(500).send({ error }));
});


// DELETE: Delete a entry in Microphones
router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return MicrophoneModel.destroy({ where: { id } })
    .then((microphone) => {
      if (microphone === 1)
      {
        res.status(200).send({code: microphone, status:"Deleted successfully"})
      }
      else
      {
        res.status(200).send({code: 0, status:"No rows deleted"})
      }
    })
    .catch((error) => res.status(500).send({ error }));
});

module.exports = router;
